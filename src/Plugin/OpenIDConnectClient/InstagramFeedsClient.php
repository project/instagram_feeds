<?php

namespace Drupal\instagram_feeds\Plugin\OpenIDConnectClient;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Instagram OpenID Connect client.
 *
 * Implements OpenID Connect Client plugin for Instagram.
 *
 * @OpenIDConnectClient(
 *   id = "instagram_feeds",
 *   label = @Translation("Instagram (feeds)")
 * )
 */
class InstagramFeedsClient extends OpenIDConnectClientBase {

  /**
   * Instagram fields.
   *
   * @var array
   */
  protected $fields = ['id', 'username', 'account_type', 'media_count'];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'save_instagram_username' => TRUE,
      'save_instagram_avatar' => TRUE,
      'avatar_uri_scheme' => 'public',
      'avatar_directory' => 'instagram_avatars',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['client_id']['#title'] = $this->t('Instagram App ID (Client) ID');
    $form['client_secret']['#title'] = $this->t('Instagram App Secret (Client) ID');
    $form['client_secret']['#type'] = 'textfield';
    $t_opts = ['@url' => 'https://developers.facebook.com/apps/'];
    $form['description'] = [
      '#markup' => '<div class="description">' . $this->t('Set up your app in <a href="@url" target="_blank">my apps</a> on Facebook. Setup a <b>Instagram Basic Display</b> product in your Facebook app.', $t_opts) . '</div>',
    ];

    $form['save_instagram_username'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save Instagram username in user account'),
      '#default_value' => $this->configuration['save_instagram_username'],
    ];

    $form['save_instagram_avatar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save Instagram avatar in user account'),
      '#default_value' => $this->configuration['save_instagram_avatar'],
    ];

    $scheme_options = \Drupal::service('stream_wrapper_manager')->getNames(StreamWrapperInterface::WRITE_VISIBLE);
    $form['avatar_uri_scheme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Avatar download destination'),
      '#options' => $scheme_options,
      '#default_value' => $this->configuration['avatar_uri_scheme'],
      '#required' => TRUE,
      '#description' => $this->t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
    ];

    $form['avatar_directory'] = [
      '#title' => $this->t('Avatar directory'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => $this->t('Where to store avatars, for example: "instagram_avatars".'),
      '#default_value' => $this->configuration['avatar_directory'],
      '#element_validate' => ['\Drupal\file\Plugin\Field\FieldType\FileItem::validateDirectory'],
    ] + $this->getTokenDescription();

    return $form;
  }

  /**
   * Gets token description if module is enabled or empty array.
   *
   * @return array
   *   Token description array.
   */
  protected function getTokenDescription(): array {
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $types = [
        'site',
        'random',
        'current-date',
        'current-user',
        'instagram-account',
      ];
      $tree = [
        '#theme' => 'token_tree_link',
        '#token_types' => $types,
        '#global_types' => FALSE,
        '#click_insert' => TRUE,
      ];
      $t_opts = ['@tokens_link' => \Drupal::service('renderer')->render($tree)];
      return [
        '#field_suffix' => $this->t('This field supports tokens. @tokens_link', $t_opts),
        '#element_validate' => ['token_element_validate'],
        '#token_types' => $types,
      ];
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(): array {
    return [
      'authorization' => 'https://api.instagram.com/oauth/authorize',
      'token' => 'https://api.instagram.com/oauth/access_token',
      'userinfo' => 'https://graph.instagram.com/me',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function authorize(string $scope = 'openid email', array $additional_params = []): Response {
    // Use Instagram specific authorisations.
    return parent::authorize('user_profile,user_media');
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveUserInfo(string $access_token): ?array {
    $request_options = [
      'query' => [
        'access_token' => $access_token,
        'fields' => implode(',', $this->fields),
      ],
      'headers' => [
        'Accept' => 'application/json',
      ],
    ];

    try {
      $response = $this->httpClient->get($this->getEndpoints()['userinfo'], $request_options);
      $userinfo = Json::decode((string) $response->getBody());

      // Make sure the result is an array before returning it.
      if (is_array($userinfo)) {
        $userinfo['sub'] = $userinfo['id'];
        return $userinfo;
      }
    }
    catch (\Exception $e) {
      $variables = [
        '@message' => 'Could not retrieve user profile information',
        '@error_message' => $e->getMessage(),
      ];
      $this->loggerFactory->get('openid_connect_' . $this->pluginId)
        ->error('@message. Details: @error_message', $variables);
    }
    return NULL;
  }

}
